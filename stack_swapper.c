#include <stdio.h>
#include <stdlib.h>
#include "stack/stack.h"

// William Doyle
// March 13th 2020
// The Title Of This Work: "Fun Loop Program"
// GNU General Public License v2.0
// gcc stack_swapper.c stack/stack.c

int main (void) {
	static int main_number = 0;
	main_number++;
	if (main_number > 10){
		return 0;
	}
	struct stack * things;
	things = make_stack();
	for (int i = 0; i < 0xF; i++){
		printf("%d ", main_number);
		things->push(things, (void*)main);
	}
	printf("\n");
	struct stack * swap_stack;
	swap_stack = make_stack();
	for (int i = 0; i < 0xF; i++){
		printf("%d ", main_number);
		int (*func)(void);
		func = things->pop(things);
		func();
		swap_stack->push(swap_stack, func);
	}
	for (int i = 0; i < 0xF; i++){
		printf("%d ", main_number);
		things->push(things, swap_stack->pop(swap_stack));
	}
	printf("\n\n");
	destroy_stack(swap_stack);
	destroy_stack(things);
}
